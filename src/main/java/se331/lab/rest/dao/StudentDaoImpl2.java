package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("MyDao")
@Repository
public class StudentDaoImpl2 implements StudentDao{
    List<Student> students;
    public StudentDaoImpl2(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(66L)
                .studentId("SE-5002")
                .name("Kull")
                .surname("Thana")
                .gpa(2.57)
                .image("https://static.vibe.com/files/2017/02/Roll-Safe-1485964928-compressed.jpg")
                .penAmount(10)
                .description("...")
                .build());
    }

    @Override
    public List<Student> getAllStudent() {
        log.info("My dao is called");
        return students;
    }

    @Override
    public Student findById(Long id) {
        return students.get((int) (id -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}